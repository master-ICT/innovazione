# Innovation Management 2018
**Gruppo**: Caserio, Osmani, Scafesi, Socco


# Idea
Piattaforma per fare la spesa on-line nella maniera più conveniente possibile. Vengono monitorate le offerte delle varie catene di supermercati. *Raider* che assembla la spesa nella maniera più economica e la consegna a domicilio. Modello con subscription.

# Workshop 4P

Esempi di innovazione incrementale e radicale legati a Product, Process, Position, Paradigm.

|  PRODUCT              |INCREMENTAL INNOVATION                          |RADICAL INNOVATION                         |
|----------------|-------------------------------|-----------------------------|
||Carte Contactless            |Microsoft: software come prodottto e non come accessorio dell'hardware            |
||Da HardDisk a SSD         |Xerox: WYSIWYG          |
||Esperienze AirBnb|Satispay 
||PosteMobile/PostePay            |Caffè con cialde           |


--

|  PROCESS              |INCREMENTAL INNOVATION                          |RADICAL INNOVATION                         |
|----------------|-------------------------------|-----------------------------|
||Sky digitale terrestre            |Piattaforma per opere letterarie            |
||         |Kickstarter          |

--

|  POSITION              |INCREMENTAL INNOVATION                          |RADICAL INNOVATION                         |
|----------------|-------------------------------|-----------------------------|
||Tariffe di Spotify per studenti            |MTV: da musicale a generalista            |
||Vodafone: numero di telefono di casa ricaricabile         |          |


--

|  PARADIGM             |INCREMENTAL INNOVATION                          |RADICAL INNOVATION                         |
|----------------|-------------------------------|-----------------------------|
||Windows10 "as a service"            |Realtà virtuale            |
||         |Videoclip Musicali          |


